//
//  WeatherProvider.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 12/15/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherProvider: openWeatherMapDelegate {
    
    func requestData(completion: ((_ data: [Int]) -> Void)) {
          DispatchQueue.main.async {
      //      self.openWeather.getForecastCityID()
            }
        completion(tempArray)
    }
    
    
    func updateWeatherInfo(weatherJSON: JSON) { }
    var openWeather = openWeatherMap()
    
    //MARK: Forecast delegate
    var tempFirst: Int = 1000
    var tempSec: Int = 1000
    var tempThird: Int = 1000
    var tempFourth: Int = 1000
    var tempFifth: Int = 1000
    var tempArray:[Int] = [0]
    func updateForecast(weatherJSON: JSON) {
        if let tempFirstRes = weatherJSON["list"][5]["main"]["temp"].double {
            tempFirst = self.openWeather.converteTemp(temperature: tempFirstRes)
            tempArray[0]=tempFirst
         //   print(tempFirst)
        }
        if let tempSecRes = weatherJSON["list"][13]["main"]["temp"].double {
            tempSec = self.openWeather.converteTemp(temperature: tempSecRes)
            tempArray[1]=tempFirst
            //  print(tempSec)
        }
        if let tempThirdRes = weatherJSON["list"][21]["main"]["temp"].double {
            tempThird = self.openWeather.converteTemp(temperature: tempThirdRes)
            tempArray[2]=tempFirst
            //  print(tempThird)
        }
        if let tempFourthRes = weatherJSON["list"][29]["main"]["temp"].double {
            tempFourth = self.openWeather.converteTemp(temperature: tempFourthRes)
            tempArray[3]=tempFirst
            //  print(tempFourth)
        }
        if let tempFifthRes = weatherJSON["list"][37]["main"]["temp"].double {
            tempFifth = self.openWeather.converteTemp(temperature: tempFifthRes)
            tempArray[4]=tempFirst
            //print(tempFifth)
        }
    }
}
