//
//  View.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 10/16/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit

class OutPutWeatherModel {
    var temp: Int?
    var name: String?
    var icon: UIImage?
    init(temp: Int?, name: String?, icon: UIImage?) {
        self.temp = temp
        self.icon = icon
        self.name = name 
    }
}
