//
//  CollectionViewCell.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 10/31/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var nameCityLbl: UILabel!
    
    @IBOutlet weak var currentTempLbl: UILabel!
    
    
    func setNameCity(name: String) {
            nameCityLbl.text = name
    }
    
    func setTempCity(temp: String) {
        currentTempLbl.text = "\(temp) °C"
    }
    
    func setWeatherIcon(icon: UIImage) {
        weatherIcon.image = icon
    }
    
}
