//
//  CollectionCellForecast.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 12/10/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit

class ForecastCell: UICollectionViewCell {
    
    @IBOutlet weak var firstDayImg: UIImageView!
    
    @IBOutlet weak var firstDayLbl: UILabel!
    
    func setFirstImg(icon: UIImage) {
        firstDayImg.image = icon
    }
    
    func setFirstTemp(temp: String) {
        firstDayLbl.text = temp
    }

}
