//
//  StartViewController.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 10/25/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation



class StartViewController: UIViewController, openWeatherMapDelegate, CLLocationManagerDelegate {
  
    var locationManager: CLLocationManager!
    let manager = CLLocationManager()
    
    var openWeather = openWeatherMap()
    var openForecast = openWeatherForecast()
    
    var myLocation: CLLocationCoordinate2D?
    
    @IBAction func locationBtn(_ sender: Any) {
        print("1")
//        self.openWeather.getWeatherCityGeo(geo: myLocation!)
    //    self.openWeather.getWeatherCityGeo(geo: <#T##CLLocationCoordinate2D#>)
        self.openWeather.getWeatherCityGeo(geo: myLocation!)
    }
    
   
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
         myLocation = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        print(myLocation!.longitude,myLocation!.latitude)
    //    self.openWeather.getWeatherCityGeo(geo: myLocation!)
        print("2")
       // manager.stopUpdatingLocation()
    }
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let forecastData = WeatherProvider()
    private func useData(data: [Int]) {
        print(data)
    }

 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//         var statusBarColor: UIStatusBarStyle {
//            return .lightContent
//        }
//        forecastData.requestData { [weak self] (data:[Int]) in
//            self?.useData(data: data)
//        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
//        var preferredStatusBarStyle: UIStatusBarStyle {
//            return .lightContent
//        }
    //    UIApplication.shared.statusBarStyle?.backgroundColor = UIColor.white
//        var statusBarColor: UIStatusBarStyle {
//            return .lightContent
//        }
        print("Start View")
//        forecastData.requestData { [weak self] (data: [Int]) in
//            self?.useData(data: data)
//         //   print(data)
//        }
//        forecastData.requestData { [weak self] (data: [Int]) in
////            self?.useData(data: data)
//            print(data)
//        }
        
        //MARK: - Init location manager        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        
        //MARK: - Init collectionView
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.allowsMultipleSelection = true
        
        
        self.openForecast.delegate = self
        self.openForecast.getForecastCityID()
        
        
        self.openWeather.delegate = self
        self.openWeather.getWeatherCityID()
//        self.openWeather.getWeatherCityGeo(geo: <#T##CLLocationCoordinate2D#>)
        
//        self.openWeather.getWeatherCityGeo(geo: myLocation!)
        
   //     self.data.delegate = self
        
    }
    
   
    var nameCity: String = "nil"
    var iconCity: UIImage = #imageLiteral(resourceName: "Wind")
    var iconCityBig: UIImage = #imageLiteral(resourceName: "Wind")
    var tempCity: Int = 1000
    var humidity: Int = -1
    var rainChance: Int = -1
    var wind: Double = 0.0
    var rain: Double = -1.0
    
    
    //MARK:- OpenWeatherMapDelegate
    func updateWeatherInfo(weatherJSON: JSON) {
        
        if let name = weatherJSON["name"].string {
            nameCity = name
       //     print(name)
        }
        if let tempResult = weatherJSON["main"]["temp"].double {
            let tempC = self.openWeather.converteTemp(temperature: tempResult)
            tempCity=tempC
        }
        if let humidityResult = weatherJSON["main"]["humidity"].int {
            humidity = humidityResult
        }
        if let windResult = weatherJSON["wind"]["speed"].double {
            wind = windResult
        }
        if let weatherStatus = weatherJSON["weather"][0]["id"].int {
            //  print(weatherStatus)
            iconCity = self.openWeather.weatherIconBig(condition: weatherStatus)
        }
        collectionView.reloadData()
    }
    
    var tempFirst: Int = 1000
    var tempSec: Int = 1000
    var tempThird: Int = 1000
    var tempFourth: Int = 1000
    var tempFifth: Int = 1000
   
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let cityVC = segue.destination as? CityController else {return}
        cityVC.iconImage = iconCity
        cityVC.cityNameCurrent = nameCity
        cityVC.tempCityCurrent = "\(tempCity) °C"
        cityVC.humidityCurrent = "\(humidity) %"
        cityVC.windCurrent = "\(Int(wind)) m/s"
        cityVC.rainCurrent = "\(Int(rain*10)) mm"
    }
}

//MARK: - CollectionViewDataSource
extension StartViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! MainCollectionViewCell
        cell.setNameCity(name: nameCity)
        cell.setWeatherIcon(icon: iconCity)
        cell.setTempCity(temp: String(tempCity))
        return cell
    }
}

//MARK: - CollectionViewDelegate
extension StartViewController: UICollectionViewDelegate {
    
}

//MARK: - OpenWeatherForecastDelegate
extension StartViewController: openWeatherForecastDelegete {
    func updateForecast(weatherJSON: JSON) {
        if let tempFirstRes = weatherJSON["list"][5]["main"]["temp"].double {
            tempFirst = self.openWeather.converteTemp(temperature: tempFirstRes)
            //     print(tempFirst)
        }
        if let tempSecRes = weatherJSON["list"][13]["main"]["temp"].double {
            tempSec = self.openWeather.converteTemp(temperature: tempSecRes)
            //  print(tempSec)
        }
        if let tempThirdRes = weatherJSON["list"][21]["main"]["temp"].double {
            tempThird = self.openWeather.converteTemp(temperature: tempThirdRes)
            //  print(tempThird)
        }
        if let tempFourthRes = weatherJSON["list"][29]["main"]["temp"].double {
            tempFourth = self.openWeather.converteTemp(temperature: tempFourthRes)
            //  print(tempFourth)
        }
        if let tempFifthRes = weatherJSON["list"][37]["main"]["temp"].double {
            tempFifth = self.openWeather.converteTemp(temperature: tempFifthRes)
            //print(tempFifth)
        }
        if let weatherStatus = weatherJSON["list"][5]["weather"][0]["id"].int {
            //  print(weatherStatus)
            
            //   iconCity = self.openWeather.weatherIconBig(condition: weatherStatus)
        }
        if let weatherStatus = weatherJSON["list"][13]["weather"][0]["id"].int {
            // print(weatherStatus)
            //   iconCity = self.openWeather.weatherIconBig(condition: weatherStatus)
        }
        if let weatherStatus = weatherJSON["list"][21]["weather"][0]["id"].int {
            //print(weatherStatus)
            //  iconCity = self.openWeather.weatherIconBig(condition: weatherStatus)
        }
        if let weatherStatus = weatherJSON["list"][29]["weather"][0]["id"].int {
            //   print(weatherStatus)
            //   iconCity = self.openWeather.weatherIconBig(condition: weatherStatus)
        }
        if let weatherStatus = weatherJSON["list"][37]["weather"][0]["id"].int {
            //  print(weatherStatus)
            //   iconCity = self.openWeather.weatherIconBig(condition: weatherStatus)
        }
    }
}
