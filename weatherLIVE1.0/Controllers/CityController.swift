//
//  CityController.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 11/8/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON



class CityController: UIViewController, UICollectionViewDelegate {
   
    
    @IBOutlet weak var humidityLbl: UILabel!
    
    @IBOutlet weak var rainChanceLbl: UILabel!
    
    @IBOutlet weak var weatherIcon: UIImageView!
    
    @IBOutlet weak var cityNameLbl: UILabel!
    
    @IBOutlet weak var tempCityLbl: UILabel!
    
    @IBOutlet weak var cityNameNavItem: UINavigationItem!
    
    @IBOutlet weak var windLbl: UILabel!
    
    var iconImage: UIImage!
    var tempCityCurrent: String!
    var cityNameCurrent: String!
    var humidityCurrent: String!
    var windCurrent: String!
    var rainCurrent: String!
    
    @IBOutlet weak var foreCastCollection: UICollectionView!
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    private let forecastData = WeatherProvider()
    private func useData(data: [Int]) {
        print(data)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        

        foreCastCollection.delegate = self
        foreCastCollection.dataSource = self
        
        
//        forecastData.requestData { [weak self] (data: [Int]) in
//            self!.useData(data: data)
//        }
        
      
        
        
        weatherIcon.image = iconImage
      //  cityNameLbl.text = cityNameCurrent
        tempCityLbl.text = tempCityCurrent
        cityNameNavItem.title = cityNameCurrent
        humidityLbl.text = humidityCurrent
        windLbl.text = windCurrent
        rainChanceLbl.text = rainCurrent
    }
    
}


extension CityController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ForecastCell", for: indexPath) as! ForecastCell
        
        let jopa = #imageLiteral(resourceName: "Wind")
      //  if(cell.index(ofAccessibilityElement: 1) == 1){
            
        cell.setFirstImg(icon: jopa)
        cell.setFirstTemp(temp: String(indexPath.row))
        return cell
        
    }
}
