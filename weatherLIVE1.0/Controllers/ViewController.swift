//
//  ViewController.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 10/16/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON

class ViewController: UIViewController {

    var cities = [City]() {
        
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    var searchCities = [City]()
    
    var isSearching = false
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.configure()
        
        Networking.shared.parseLocalJson(completion: { (city, error) in
            
            if error != nil{
                return
            } else if let city = city{
                self.cities = city
               // print(self.cities.count)
            }
        })
        
    }

    //MARK: - Private
    fileprivate func configure() {
        
        self.searchBar.delegate = self
    }
    
}


extension ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (self.isSearching ? self.searchCities : self.cities).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = self.isSearching ? searchCities[indexPath.row].name : cities[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var name2 = String()
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let name = cities[indexPath.row]
        name2 = String(name.id)
        var jopa = openWeatherMap()
         jopa.cityID = name2
       // let jopa = openWeatherMap(cityID: name2)
        print(jopa.cityID)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondVC = storyboard.instantiateViewController(withIdentifier: "startview") as! StartViewController
        self.present(secondVC,animated: true,completion: nil)
    }
}

extension ViewController: UITableViewDelegate {
    
}

extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.isSearching = (searchText != "")
        
        guard self.isSearching else {
            
            self.searchCities = [City]()
            self.tableView.reloadData()

            return
        }
        
        self.searchCities = self.cities.filter({
            $0.name.contains(searchText)
        })
        
        self.tableView.reloadData()
    }
}
