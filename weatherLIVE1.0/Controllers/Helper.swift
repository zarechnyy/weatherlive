//
//  Helper.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 1/4/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    
    static let shared = Helper()
    
    private init() {
        
    }
    
    func getCelcium(tempKelvin: Double) -> Int {
        let temC = Int(tempKelvin-273.15)
        return temC
    }
    
    func getIcon(id: Int) -> UIImage {
        var imageName: String
        switch id {
        //Drizzle
        case 300...400: imageName = "Weather5_Big"
        //Sunny
        case 800 : imageName = "Weather3_Big"
        //Few-Clouds
        case 801 : imageName = "Weather4_Big"
        //Cloudly
        case 800...900: imageName = "Weather1_Big"
        //Thunderstorm
        case 500...620: imageName = "Weather2_Big"
            
        default : imageName = "none"
        }
        
        let iconImage = UIImage(named: imageName)
        return iconImage!
    }
}
