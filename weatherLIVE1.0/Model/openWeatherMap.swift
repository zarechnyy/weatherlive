//
//  OpenWeatherMap.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 10/31/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreLocation

protocol openWeatherMapDelegate: class {
    func updateWeatherInfo(weatherJSON: JSON)
    //func updateForecast(weatherJSON: JSON)
}

class openWeatherMap {
    
    let APPID = "39b34a1edc7be26d482b3bef37a77704"
    var cityID = "2643743"
//    var cityID: [String]?
    var nameCity: String?
    var temp: Double?

    
    //https://api.openweathermap.org/data/2.5/forecast?
    
    weak var delegate: openWeatherMapDelegate!
    
    func getWeatherCityGeo(geo: CLLocationCoordinate2D) {
        
        let params = ["lat":geo.latitude,"lon":geo.longitude]
        request("https://api.openweathermap.org/data/2.5/weather?lat=\(geo.latitude)&lon=\(geo.longitude)&appid=\(APPID)", method: .get, parameters: params).responseJSON { (json) in
            let weatherJSON = JSON(json.data!)
            DispatchQueue.main.async {
                self.delegate.updateWeatherInfo(weatherJSON: weatherJSON)
            }
        }
    }
    //709717
    //2172797
    

   // 709717
    
    func getWeatherCityID() {
        request("https://api.openweathermap.org/data/2.5/weather?id=\(cityID)&appid=\(APPID)").responseJSON { (json) in
            let weatherJSON = JSON(json.data!)
            print("Open weather map id \(self.cityID)")
                DispatchQueue.main.async {
                    
                    self.delegate.updateWeatherInfo(weatherJSON: weatherJSON)
                }
            }
        }
    //MARK:-ConverteToCelcium
    func converteTemp(temperature:Double) -> Int {
        let temC = Int(temperature-273.15)
        return temC
        }
    

    //MARK:-Setting up weather icon
    func weatherIconBig(condition: Int) -> UIImage {
        var imageName: String
        switch condition {
        //Drizzle
        case 300...400: imageName = "Weather5_Big"
        //Sunny
        case 800 : imageName = "Weather3_Big"
        //Few-Clouds
        case 801 : imageName = "Weather4_Big"
        //Cloudly
        case 800...900: imageName = "Weather1_Big"
        //PizdecRain
        case 500...620: imageName = "Weather2_Big"
        default : imageName = "none"
        }
        
        let iconImage = UIImage(named: imageName)
        return iconImage!
    }

}
    

    

    //MARK:- Getting time
    func timeFromUnix(unixTime: Int)->String {
        
        let timeInSecond = TimeInterval(unixTime)
        let weatherDate = Date(timeIntervalSince1970: timeInSecond)
        
        let dataFormatter = DateFormatter()
        dataFormatter.dateFormat = "HH:mm"
        
        return dataFormatter.string(from: weatherDate)
    }

