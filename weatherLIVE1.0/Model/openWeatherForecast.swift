//
//  openWeatherForecast.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 1/4/19.
//  Copyright © 2019 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol openWeatherForecastDelegete: class {
    func updateForecast(weatherJSON: JSON)
}

class openWeatherForecast {
    
    
    let APPID = "39b34a1edc7be26d482b3bef37a77704"
    var cityID = "2643743"
    
    weak var delegate: openWeatherForecastDelegete!
    
    func getForecastCityID() {
        request("https://api.openweathermap.org/data/2.5/forecast?id=\(cityID)&appid=\(APPID)").responseJSON { (json) in
            let weatherJSON = JSON(json.data!)
            DispatchQueue.main.async {
                self.delegate.updateForecast(weatherJSON: weatherJSON)
            }
        }
    }

    
}
