//
//  Model.swift
//  weatherLIVE1.0
//
//  Created by Yaroslav Zarechnyy on 10/16/18.
//  Copyright © 2018 Yaroslav Zarechnyy. All rights reserved.
//

import Foundation
import SwiftyJSON


enum SError: Error {
    case wrongJSONDecode
}

struct Coordinates: Decodable{

    let lat: Double?
    let lon: Double?

    enum CoordinatesKey: String, CodingKey {
        case lat
        case lon
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CoordinatesKey.self)

        self.lon = try container.decodeIfPresent(Double.self, forKey: .lon)
        self.lat = try container.decodeIfPresent(Double.self, forKey: .lat)
    }
}

struct City: Decodable {

    let id: Int
    let name: String
    let country: String
    let coord: Coordinates
}




class Networking {
    
    static let shared = Networking()
    
    //MARK: Initialization
    private init() {
        
    }
    
    func parseLocalJson(completion: @escaping ([City]?, Error?)-> Void) {

        if let path = Bundle.main.path(forResource: "city.list", ofType: "json") {

            let url = URL(fileURLWithPath: path)
            URLSession.shared.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
                guard let data = data else {return}
                do{
                    let decoder = JSONDecoder()
                    let cities = try decoder.decode([City].self,from:data)
                    completion(cities ,nil)
                }catch{
                    completion(nil,error)
                }
            }.resume()
        }
    }
}
